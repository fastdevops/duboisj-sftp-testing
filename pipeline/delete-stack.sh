#!/bin/bash -ex

# Set environment for aws-cli commands
export STACK_NAME=${1}
export ARTIFACT_PATH=${2}
export CLEAN_S3_ARTIFACTS_BUCKET=${3}

aws cloudformation delete-stack --stack-name "${STACK_NAME}" --region "${AWS_REGION}"

if [ "${CLEAN_S3_ARTIFACTS_BUCKET}" == "true" ]
then
  aws s3 rm "s3://${S3_ARTIFACTS_BUCKET}/${ARTIFACT_PATH}"  --recursive
fi
