#!/bin/bash -ex

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

# Install tools
"${SCRIPTPATH}/install-tools.sh"

# Set environment for aws-cli commands
ARTIFACT_PATH=${1}

# package PR
aws cloudformation package  \
    --template-file template.yaml \
    --s3-bucket "${S3_ARTIFACTS_BUCKET}" \
    --s3-prefix "${ARTIFACT_PATH}" \
    --output-template-file packaged.yaml

aws s3 cp packaged.yaml "s3://${S3_ARTIFACTS_BUCKET}/${ARTIFACT_PATH}/packaged-template.yaml"