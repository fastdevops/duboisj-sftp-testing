#!/bin/bash -ex

# Install the AWS CLI
#
## Installation directory
CLI_INSTALL_DIR=~/awscli


if [ ! -f "${CLI_INSTALL_DIR}/bin/aws" ]
then
  curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
  unzip awscli-bundle.zip
  ./awscli-bundle/install -i $CLI_INSTALL_DIR
fi
## Symbolic link the cli binary to a location in the PATH
if [ ! -L /usr/local/bin/aws ]
then
  ln -s "${CLI_INSTALL_DIR}/bin/aws" /usr/local/bin/aws
fi
aws --version
