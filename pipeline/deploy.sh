#!/bin/bash -ex

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

# Install tools
"${SCRIPTPATH}/install-tools.sh"

# Set environment for aws-cli commands
# Set environment for aws-cli commands
STACK_NAME=${1}
DEPLOYMENT_ENVIRONMENT=${2}
BUILD_NUMBER=${3}
ARTIFACT_PATH={4}

if [ ! -f packaged.yaml ]
then
  aws s3 cp "s3://${S3_ARTIFACTS_BUCKET}/${ARTIFACT_PATH}/packaged-template.yaml" packaged.yaml
fi

# deploy PR
aws cloudformation deploy --template-file packaged.yaml --stack-name "${STACK_NAME}" \
    --capabilities CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND \
    --parameter-overrides DeploymentEnvironment="${DEPLOYMENT_ENVIRONMENT}" \
    --region "${AWS_REGION}"
