#!/bin/bash -ex

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

# Set environment for aws-cli commands
STACK_NAME=${1}
DEPLOYMENT_ENVIRONMENT=${2}
ARTIFACT_PATH=${3}
BUILD_NUMBER=${4}
LAMBDA_NAME_PREFIX=${5}
# Package app
"${SCRIPTPATH}/package.sh" "${ARTIFACT_PATH}"

# Deploy and test app
"${SCRIPTPATH}/deploy.sh" \
    "${STACK_NAME}" \
    "${DEPLOYMENT_ENVIRONMENT}" \
    "${BUILD_NUMBER}" \
    "${ARTIFACT_PATH}" \
    "${LAMBDA_NAME_PREFIX}"