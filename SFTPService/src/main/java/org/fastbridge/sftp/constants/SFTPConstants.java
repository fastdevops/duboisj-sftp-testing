/**
 * SFTPConstants
 * Copyright (c) 2019, FastBridge Learning LLC
 * Created on 10/8/19
 */
package org.fastbridge.sftp.constants;

/**
 * @author kushalmittal
 */
public class SFTPConstants {

    /**
     * VARS
     */
    public static final String UTF8_ENCODING = "UTF-8";

    public static final String SFTP_STORE_DOCS_BUCKET = "sftp-store-docs";
    public static final String FULL_STAFF_README = "full_staff_roster_readme.txt";
    public static final String FULL_STUDENT_README = "full_student_roster_readme.txt";
    public static final String INCREMENTAL_STUDENT_README = "incremental_student_roster_readme.txt";

    public static final String FULL_STUDENT_PATH = "/upload/student/full";
    public static final String FULL_STAFF_PATH = "/upload/staff";
    public static final String INCREMENTAL_STUDENT_PATH = "/upload/student/incremental";

    public static final String README_SUFFIX = "/readme.txt";
}
