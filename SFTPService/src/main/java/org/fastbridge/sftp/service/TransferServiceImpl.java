/**
 * TransferServiceImpl
 * Copyright (c) 2019, FastBridge Learning LLC
 * Created on 9/27/19
 */
package org.fastbridge.sftp.service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Objects;

import org.fastbridge.sftp.constants.SFTPConstants;

import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.identitymanagement.model.GetPolicyRequest;
import com.amazonaws.services.identitymanagement.model.GetPolicyResult;
import com.amazonaws.services.identitymanagement.model.GetPolicyVersionRequest;
import com.amazonaws.services.identitymanagement.model.GetPolicyVersionResult;
import com.amazonaws.services.identitymanagement.model.Policy;
import com.amazonaws.services.identitymanagement.model.PolicyVersion;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.transfer.AWSTransferAsync;
import com.amazonaws.services.transfer.AWSTransferAsyncClientBuilder;
import com.amazonaws.services.transfer.model.CreateUserRequest;
import com.amazonaws.services.transfer.model.CreateUserResult;
import com.amazonaws.services.transfer.model.DeleteSshPublicKeyRequest;
import com.amazonaws.services.transfer.model.DeleteSshPublicKeyResult;
import com.amazonaws.services.transfer.model.DescribeUserRequest;
import com.amazonaws.services.transfer.model.DescribeUserResult;
import com.amazonaws.services.transfer.model.DescribedUser;
import com.amazonaws.services.transfer.model.ImportSshPublicKeyRequest;
import com.amazonaws.services.transfer.model.ImportSshPublicKeyResult;
import com.amazonaws.services.transfer.model.ResourceNotFoundException;

/**
 * @author kushalmittal
 */
public class TransferServiceImpl implements TransferService {

    public DescribedUser getTransferUserDescription(String username, String serverID) {
        AWSTransferAsync awsTransferAsyncClient = AWSTransferAsyncClientBuilder.standard().build();
        DescribeUserRequest describeUserRequest = new DescribeUserRequest().withUserName(username).withServerId(serverID);
        DescribedUser describedUser = null;
        try {
            DescribeUserResult describeUserResult = awsTransferAsyncClient.describeUser(describeUserRequest);
            if (Objects.nonNull(describeUserResult)) {
                describedUser = describeUserResult.getUser();
            }
        } catch (ResourceNotFoundException e) {
            // do nothing
        }
        return describedUser;
    }

    @Override
    public void addTransferUserSSHPublicKey(String username, String serverID, String sshPublicKey) {
        AWSTransferAsync awsTransferAsyncClient = AWSTransferAsyncClientBuilder.standard().build();
        ImportSshPublicKeyRequest importSshPublicKeyRequest = new ImportSshPublicKeyRequest().withUserName(username).withServerId(serverID).withSshPublicKeyBody(sshPublicKey);
        ImportSshPublicKeyResult importSshPublicKeyResult = awsTransferAsyncClient.importSshPublicKey(importSshPublicKeyRequest);
    }

    @Override
    public void deleteUserSSHPublicKey(String username, String serverID, String sshPublicKeyId) {
        AWSTransferAsync awsTransferAsyncClient = AWSTransferAsyncClientBuilder.standard().build();
        DeleteSshPublicKeyRequest deleteSshPublicKeyRequest = new DeleteSshPublicKeyRequest().withUserName(username).withServerId(serverID).withSshPublicKeyId(sshPublicKeyId);
        DeleteSshPublicKeyResult deleteSshPublicKeyResult = awsTransferAsyncClient.deleteSshPublicKey(deleteSshPublicKeyRequest);
    }

    @Override
    public void createTransferUserRequest(String username, String serverID, String bucketName, String roleArn, String scopeDownPolicyArn, String sshPublicKey) throws UnsupportedEncodingException {
        AWSTransferAsync awsTransferAsyncClient = AWSTransferAsyncClientBuilder.standard().build();
        String homeFolder = String.join("/", "", bucketName, username);
        CreateUserRequest createUserRequest = new CreateUserRequest()
                .withUserName(username).withServerId(serverID)
                .withHomeDirectory(homeFolder).withRole(roleArn).withPolicy(getPolicyDocument(scopeDownPolicyArn))
                .withSshPublicKeyBody(sshPublicKey);
        CreateUserResult createUserResult = awsTransferAsyncClient.createUser(createUserRequest);
        /**
         * add readme files on the roster upload directories
         */
        AmazonS3 amazonS3 = AmazonS3ClientBuilder.defaultClient();
        amazonS3.copyObject(SFTPConstants.SFTP_STORE_DOCS_BUCKET, SFTPConstants.FULL_STAFF_README, bucketName, username + SFTPConstants.FULL_STAFF_PATH + SFTPConstants.README_SUFFIX);
        amazonS3.copyObject(SFTPConstants.SFTP_STORE_DOCS_BUCKET, SFTPConstants.FULL_STUDENT_README, bucketName, username + SFTPConstants.FULL_STUDENT_PATH + SFTPConstants.README_SUFFIX);
        amazonS3.copyObject(SFTPConstants.SFTP_STORE_DOCS_BUCKET, SFTPConstants.INCREMENTAL_STUDENT_README, bucketName, username + SFTPConstants.INCREMENTAL_STUDENT_PATH + SFTPConstants.README_SUFFIX);
    }

    public String getPolicyDocument(String policyArn) throws UnsupportedEncodingException {
        final AmazonIdentityManagement iam =
                AmazonIdentityManagementClientBuilder.defaultClient();
        GetPolicyRequest getPolicyRequest = new GetPolicyRequest()
                .withPolicyArn(policyArn);
        GetPolicyResult getPolicyResult = iam.getPolicy(getPolicyRequest);
        Policy policy = getPolicyResult.getPolicy();

        GetPolicyVersionRequest getPolicyVersionRequest = new GetPolicyVersionRequest();
        getPolicyVersionRequest.setPolicyArn(policy.getArn());
        getPolicyVersionRequest.setVersionId(policy.getDefaultVersionId());

        GetPolicyVersionResult getPolicyVersionResult = iam.getPolicyVersion(getPolicyVersionRequest);
        PolicyVersion policyVersionResult = getPolicyVersionResult.getPolicyVersion();
        return URLDecoder.decode(policyVersionResult.getDocument(), SFTPConstants.UTF8_ENCODING);
    }
}
