/**
 * TransferService
 * Copyright (c) 2019, FastBridge Learning LLC
 * Created on 9/27/19
 */
package org.fastbridge.sftp.service;

import java.io.UnsupportedEncodingException;

import com.amazonaws.services.transfer.model.DescribedUser;

/**
 * @author kushalmittal
 */
public interface TransferService {

    /**
     * Describes a user on the transfer server.
     *
     * @param username
     * @param serverID
     * @return A user on the transfer server. Null if user not found.
     */
    DescribedUser getTransferUserDescription(String username, String serverID);

    /**
     * Adds a Public SSH Key to a transfer user.
     *
     * @param username
     * @param serverID
     * @param sshPublicKey
     */
    void addTransferUserSSHPublicKey(String username, String serverID, String sshPublicKey);

    /**
     * Deletes a public key for a user on a server with a specified keyid.
     * @param username
     * @param serverID
     * @param sshPublicKeyId
     */
    void deleteUserSSHPublicKey(String username, String serverID, String sshPublicKeyId);

    /**
     * 1. Adds a user account on the SFTP server
     * 2. Configures the roster upload paths and places readme files.
     * @param username
     * @param serverID
     * @param bucketName
     * @param roleArn
     * @param scopeDownPolicyArn
     * @param sshPublicKey
     * @throws UnsupportedEncodingException
     */
    void createTransferUserRequest(String username, String serverID, String bucketName, String roleArn, String scopeDownPolicyArn, String sshPublicKey) throws UnsupportedEncodingException;

    /**
     * gets a policy document using the policyarn
     * @param policyArn
     * @return policy in JSON format.
     * @throws UnsupportedEncodingException
     */
    String getPolicyDocument(String policyArn) throws UnsupportedEncodingException;
}
