/**
 * CreateUserRequest
 * Copyright (c) 2019, FastBridge Learning LLC
 * Created on 9/26/19
 */
package org.fastbridge.sftp.dto;

/**
 * @author kushalmittal
 */
public class AddUpdateSFTPUserRequest {

    String username;

    String sshPublicKey;

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getSshPublicKey() {
        return sshPublicKey;
    }

    public void setSshPublicKey(String sshPublicKey) {
        this.sshPublicKey = sshPublicKey;
    }


}
