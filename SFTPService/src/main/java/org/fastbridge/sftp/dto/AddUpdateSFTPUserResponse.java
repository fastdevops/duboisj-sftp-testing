/**
 * AddUpdateSFTPUserResponse
 * Copyright (c) 2019, FastBridge Learning LLC
 * Created on 10/9/19
 */
package org.fastbridge.sftp.dto;

/**
 * @author kushalmittal
 */
public class AddUpdateSFTPUserResponse {

    private String result;

    public AddUpdateSFTPUserResponse(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
