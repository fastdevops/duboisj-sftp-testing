/**
 * PushRosterToSFTPServiceQueue
 * Copyright (c) 2019, FastBridge Learning LLC
 * Created on 9/26/19
 */
package org.fastbridge.sftp.lambda;

import com.amazonaws.Response;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;

/**
 * @author kushalmittal
 */
public class PushS3EventToSFTPQueue implements RequestHandler<S3EventNotification, Response> {
    @Override
    public Response handleRequest(S3EventNotification s3EventNotification, Context context) {
        LambdaLogger logger = context.getLogger();
        logger.log("roster received");
        String queueURL = System.getenv("RosterQueueURL");
        logger.log("queue: " + queueURL);
        AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();
        for (S3EventNotification.S3EventNotificationRecord record : s3EventNotification.getRecords()) {
            logger.log(record.getEventSource());
            logger.log(record.getEventName());
            String key = record.getS3().getObject().getKey();
            SendMessageRequest sendMessageRequest = new SendMessageRequest()
                    .withQueueUrl(queueURL)
                    .withMessageBody(key)
                    .withMessageGroupId(key.split("/")[0]);
            sqs.sendMessage(sendMessageRequest);
        }
        return null;
    }
}
