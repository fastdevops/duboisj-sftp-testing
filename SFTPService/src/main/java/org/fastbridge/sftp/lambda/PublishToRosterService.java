/**
 * PublishToRosterServiceQueue
 * Copyright (c) 2019, FastBridge Learning LLC
 * Created on 9/30/19
 */
package org.fastbridge.sftp.lambda;

import java.util.Map;
import java.util.Objects;

import com.amazonaws.Response;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;

/**
 * @author kushalmittal
 */
public class PublishToRosterService implements RequestHandler<String, Response> {

    @Override
    public Response handleRequest(String input, Context context) {
        LambdaLogger logger = context.getLogger();
        String url = System.getenv("QueueURL");
        logger.log("Receiving messages from " + url);
        final AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();
        ReceiveMessageRequest receive_request = new ReceiveMessageRequest()
                .withQueueUrl(url)
                .withWaitTimeSeconds(20);
        ReceiveMessageResult receiveMessageResult = sqs.receiveMessage(receive_request);
        if (Objects.nonNull(receiveMessageResult.getMessages())) {
            logger.log("Message count=" + receiveMessageResult.getMessages().size());
            for (final Message message : receiveMessageResult.getMessages()) {
                String s3FilePath = message.getBody();

                /**
                 * deleting message
                 */
                final String messageReceiptHandle = message.getReceiptHandle();
                sqs.deleteMessage(new DeleteMessageRequest(url, messageReceiptHandle));
            }

        }
        logger.log("Done");
        return null;
    }
}
