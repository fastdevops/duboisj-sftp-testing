package org.fastbridge.sftp.lambda; /**
 * org.fastbridge.sftp.user.AddSFTPUser
 * Copyright (c) 2019, FastBridge Learning LLC
 * Created on 9/25/19
 */

import java.io.UnsupportedEncodingException;
import java.util.Objects;

import org.fastbridge.sftp.dto.AddUpdateSFTPUserRequest;
import org.fastbridge.sftp.dto.AddUpdateSFTPUserResponse;
import org.fastbridge.sftp.service.TransferService;
import org.fastbridge.sftp.service.TransferServiceImpl;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.transfer.model.DescribedUser;

/**
 * @author kushalmittal
 */
public class AddUpdateSFTPUser implements RequestHandler<AddUpdateSFTPUserRequest, AddUpdateSFTPUserResponse> {

    /**
     * ENV Variables from the template
     */
    private final String TRANSFER_SERVER_ID = "TransferServerId";
    private final String SCOPE_DOWN_USER_POLICY_ARN = "ScopeDownUserPolicyArn";
    private final String SFTP_UPLOAD_BUCKET_NAME = "SFTPBucketName";
    private final String SFTP_USER_ROLE_ARN = "SFTPUserRoleArn";

    private TransferService transferService = new TransferServiceImpl();


    /**
     * @param addUpdateSFTPUserRequest
     * @param context
     * @return
     */
    @Override
    public AddUpdateSFTPUserResponse handleRequest(AddUpdateSFTPUserRequest addUpdateSFTPUserRequest, Context context) {
        LambdaLogger logger = context.getLogger();
        AddUpdateSFTPUserResponse addUpdateSFTPUserResponse = null;
        try {
            String transferServerId = System.getenv(TRANSFER_SERVER_ID);
            String scopeDownUserPolicyArn = System.getenv(SCOPE_DOWN_USER_POLICY_ARN);
            String sftpUploadBucketName = System.getenv(SFTP_UPLOAD_BUCKET_NAME);
            String sftpUserRoleArn = System.getenv(SFTP_USER_ROLE_ARN);
            String userName = addUpdateSFTPUserRequest.getUsername();
            String sshPublicKey = addUpdateSFTPUserRequest.getSshPublicKey();
            logger.log("ManageSFTPUserRequest: TransferServerId: " + transferServerId + " ScopeDownUserPolicyArn:" + scopeDownUserPolicyArn + " SFTPBucketName:" + sftpUploadBucketName
                    + " SFTPUserRoleArn: " + sftpUserRoleArn + " Username: " + userName + " sshPublicKey: " + sshPublicKey);
            DescribedUser user = transferService.getTransferUserDescription(userName, transferServerId);
            if (Objects.nonNull(user)) {
                //user already exists
                logger.log("Updating user..." + userName);
                if (user.getSshPublicKeys().size() > 0) {
                    logger.log("Deleting ssh key...");
                    user.getSshPublicKeys().stream().forEach(s -> transferService.deleteUserSSHPublicKey(userName, transferServerId, s.getSshPublicKeyId()));
                }
                transferService.addTransferUserSSHPublicKey(userName, transferServerId, sshPublicKey);
                addUpdateSFTPUserResponse = new AddUpdateSFTPUserResponse("User updated successfully.");
            } else {
                logger.log("creating new user..." + userName);
                transferService.createTransferUserRequest(userName, transferServerId, sftpUploadBucketName, sftpUserRoleArn, scopeDownUserPolicyArn, sshPublicKey);
                addUpdateSFTPUserResponse = new AddUpdateSFTPUserResponse("User created successfully.");
            }
        } catch (UnsupportedEncodingException use) {
            logger.log("invalid scope-down policy");
        } catch (Exception e) {
            logger.log("Unable to create user");
        }
        return addUpdateSFTPUserResponse;
    }
}
